class CommentsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :edit, :create, :destroy]

  def index
  end

  def show
  end

  def edit
  end

  # Create comment
  def create
    @comment = Comment.new(comment_params) # New object
    @comment.code_post = CodePost.find(session[:code_post_id]) # Set the id
    @comment.user = current_user
    if @comment.save # Save it
      redirect_to code_post_path(1)
    end
  end

  def update
  end

  def new
  end

  private

  def comment_params
    params.require(:comment).permit(:message, :code_post_id)
  end
end
