class CodePostsController < ApplicationController
  helper_method :is_owner?, :get_username # Helper method for views
  before_action :authenticate_user!, only: [:new, :edit, :create, :upvote, :destroy]
  before_action :set_code_post, only: [:destroy, :show, :edit, :get_username, :update, :upvote]

  def index
    @code_posts = CodePost.available.upvoted.all # Get all non deleted Code posts
  end

  def datatable_ajax
    render json: CodePostDatatable.new(view_context)
  end

  def upvote
    @code_post.do_upvote(current_user.id)
    respond_to do |format|
      format.js
    end
  end

  def show
    session[:code_post_id] = @code_post.id # Set accessable code post id
    @comments = @code_post.comments # Filter the comments
    @comment = Comment.new # New Comment object
  end

  def published
    # Get all non deleted and published by owner Code Posts
    @code_posts = CodePost.available.where(user_id: current_user).all
    render 'index' # Render default view
  end

  def new
    @code_post = CodePost.new # New Code Post
  end

  def edit
    redirect_to root_path unless is_owner?(@code_post)
  end

  def create
    @code_post = CodePost.new(code_post_params)
    @code_post.user_id = current_user.id # Setting whom will belong to
    @code_post.bits << current_user.id

    respond_to do |format|
      if @code_post.save
        format.html { redirect_to show_code_path(@code_post), notice: "Code created successfully!"}
        format.json { render :show, status: created, location: @code_post }
      else
        format.html { render :new }
        format.json { render json: @code_post.errors, status: :unproccesable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @code_post.update(code_post_params)
        format.html { redirect_to show_code_path(@code_post), notice: "Code updated successfully!"}
        format.json { render :show, status: created, location: @code_post }
      else
        format.html { render :edit }
        format.json { render json: @code_post.errors, status: :unproccesable_entity }
      end
    end
  end

  def destroy
    if is_owner?(@code_post) || current_user.is_admin?
      #@code_post.deleted_at = DateTime.now
      #@code_post.save
      @code_post.destroy
      respond_to do |format|
        format.html { redirect_to code_path, notice: "Code deleted successfully!"}
        format.json { head :no_content }
      end
    else
      redirect_to root_path
    end
  end

  def get_username
      @code_post.user.username.humanize
  end

  private

    # Owner of chosen CodePost?
    def is_owner?(code_post)
      code_post.user_id == current_user.id unless current_user.nil?
    end

    # Setting code_post before action
    def set_code_post
      @code_post = CodePost.friendly.find(params[:id])
    end

    # Strong params
    def code_post_params
      params.require(:code_post).permit(:title, :code,
      :tags, :code_language, :bits => [])
    end
end
