class CodePost < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  validates_uniqueness_of [:title, :code]
  validates_presence_of [:title, :code]
  scope :available, -> { where(deleted_at: nil) } # Not deleted CodePost scope
  scope :upvoted, -> { order('upvotes DESC') }
  belongs_to :user
  has_many :comments
  
  def should_generate_new_friendly_id?
    title_changed?
  end

  def get_language
    CodeLanguage.get_languages[self.code_language.to_i]
  end

  def get_language_mode_code
    CodeLanguage.get_languages_modes[self.code_language.to_i]
  end

  # User already voted.
  def bit_exists?(id)
    self.bits.include?(id)
  end

  # Add Bit to Code Post.
  def do_upvote(id = nil)
    if !id.nil? # Not nil
      unless bit_exists?(id)
        self.bits << id
        self.save
      end
    end
  end
end
