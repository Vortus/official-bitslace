class Comment < ApplicationRecord
  belongs_to :code_post
  belongs_to :user

  def user
    User.find(self.user_id)
  end
end
