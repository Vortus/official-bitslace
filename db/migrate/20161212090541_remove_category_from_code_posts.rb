class RemoveCategoryFromCodePosts < ActiveRecord::Migration[5.0]
  def change
    remove_column :code_posts, :category, :string
  end
end
