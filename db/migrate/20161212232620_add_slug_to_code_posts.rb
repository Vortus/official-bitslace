class AddSlugToCodePosts < ActiveRecord::Migration[5.0]
  def change
    add_column :code_posts, :slug, :string
    add_index :code_posts, :slug, unique: true
  end
end
