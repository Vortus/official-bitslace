class RemoveUpvotesFromCodePosts < ActiveRecord::Migration[5.0]
  def change
    remove_column :code_posts, :upvotes, :integer
  end
end
