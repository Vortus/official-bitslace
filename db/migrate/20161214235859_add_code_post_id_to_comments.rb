class AddCodePostIdToComments < ActiveRecord::Migration[5.0]
  def change
    add_column :comments, :code_post_id, :integer
  end
end
