class RemoveReputation2FromCodePosts < ActiveRecord::Migration[5.0]
  def change
    remove_column :code_posts, :reputation, :integer
  end
end
