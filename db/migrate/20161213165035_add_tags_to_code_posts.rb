class AddTagsToCodePosts < ActiveRecord::Migration[5.0]
  def change
    add_column :code_posts, :tags, :string
  end
end
