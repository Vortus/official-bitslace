class RemoveBitsFromCodePosts < ActiveRecord::Migration[5.0]
  def change
    remove_column :code_posts, :bits, :integer
  end
end
