class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.text :message
      t.integer :user_id

      t.timestamps
      t.datetime :deleted_at
    end
  end
end
