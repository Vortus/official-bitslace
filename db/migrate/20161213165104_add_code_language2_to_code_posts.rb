class AddCodeLanguage2ToCodePosts < ActiveRecord::Migration[5.0]
  def change
    add_column :code_posts, :code_language, :integer
  end
end
