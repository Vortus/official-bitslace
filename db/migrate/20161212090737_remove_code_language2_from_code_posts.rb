class RemoveCodeLanguage2FromCodePosts < ActiveRecord::Migration[5.0]
  def change
    remove_column :code_posts, :code_language, :integer
  end
end
