class RemoveDownvotesFromCodePosts < ActiveRecord::Migration[5.0]
  def change
    remove_column :code_posts, :downvotes, :integer
  end
end
