class AddBitsToCodePosts < ActiveRecord::Migration[5.0]
  def change
    add_column :code_posts, :bits, :integer, array: true, default: []
  end
end
