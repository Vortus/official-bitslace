class AddBitsToCodePost < ActiveRecord::Migration[5.0]
  def change
    add_column :code_posts, :bits, :integer
  end
end
